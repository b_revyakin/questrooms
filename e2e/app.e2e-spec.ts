import { QuestRoomsAppPage } from './app.po';

describe('quest-rooms-app App', () => {
  let page: QuestRoomsAppPage;

  beforeEach(() => {
    page = new QuestRoomsAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
