<?php

if (! defined('ENVIRONMENT')) {
    define('ENVIRONMENT', [
        'PRODUCTION' => 'production',
        'DEVELOPMENT' => 'development',
    ]);
}