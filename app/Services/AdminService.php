<?php

namespace App\Services;

use App\Models\Admin;
use DB;
use Exception;

class AdminService extends Service
{
    public function create(array $data)
    {
        try {
            DB::beginTransaction();

            $admin = Admin::create($data);

            DB::commit();
        } catch (Exception $e) {
            $this->rollbackAndLoggingException($e);

            return false;
        }

        return $admin;
    }

}