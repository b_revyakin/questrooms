<?php

namespace App\Services;

use App\Exceptions\ActionFailException;
use DB;
use Exception;
use Log;

class Service
{
    /**
     * @param Exception $e
     * @param string $message
     * @throws ActionFailException
     */
    public function rollbackAndLoggingException(Exception $e, $message = 'Server Error. Please contact with administrator.')
    {
        DB::rollBack();

        Log::error($e->getMessage());
        Log::error($e->getTraceAsString());

        throw new  ActionFailException($message ? trans($message) : $e->getMessage(), 500, $e);
    }
}