<?php

use App\Services\AdminService;
use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    private $admins = [
        ['Admin', 'admin@email.com', 'test123'],
    ];

    /**
     * @var AdminService
     */
    private $adminService;

    /**
     * AdminsTableSeeder constructor.
     * @param AdminService $adminService
     */
    public function __construct(AdminService $adminService)
    {
        $this->adminService = $adminService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->admins as $admin) {
            $this->adminService->create([
                'name' => $admin[0],
                'email' => $admin[1],
                'password' => bcrypt($admin[0]),
            ]);
        }
    }
}
