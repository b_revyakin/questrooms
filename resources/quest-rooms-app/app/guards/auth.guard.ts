import { Injectable }       from '@angular/core';
import {
    CanActivate, Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
}                           from '@angular/router';
import { AuthService }      from '../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private authService: AuthService) { }

    canActivate() {
        if (localStorage.getItem('token')) {
            // logged in so return true
            return this.authService.currentUser().then(() => {
                return true;
            });
        }

        // not logged in so redirect to login page
        this.router.navigate(['/login']);
        return false;
    }
}