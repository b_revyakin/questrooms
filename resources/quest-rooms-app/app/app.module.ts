import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AuthModule } from './auth/auth.module';
import { AppRoutesModule } from './routes.module';

import { AppComponent } from './app.component';
import {WelcomeModule} from "./welcome/welcome.module";
import {DashboardModule} from "./admin/dashboard/dashboard.module";
import {AuthService} from "./services/auth.service";
import {HttpClient} from "./http.client";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,

    AuthModule,
    WelcomeModule,
    DashboardModule,
    AppRoutesModule,
  ],
  providers: [AuthService, HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
