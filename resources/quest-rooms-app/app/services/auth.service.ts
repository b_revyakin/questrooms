import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import {Router} from "@angular/router";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import {HttpClient} from "../http.client";

@Injectable()
export class AuthService {
    public token: string;
    public user: string;

    constructor(private http: HttpClient, private router: Router) {
        // set token if saved in local storage
        this.token = localStorage.getItem('token');
    }

    login(credentials): Observable<boolean> {
        return this.http.post('/api/login', credentials)
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().token;

                if (token) {
                    // set token property
                    this.token = token;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('token', token);

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            })
            .catch(this.handleError);
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;

        localStorage.removeItem('token');
        localStorage.removeItem('user');
    }

    currentUser() {
        return this.http.get('/api/me').toPromise().then(user => {
            localStorage.setItem('user', JSON.stringify(user.json()));
        }, () => {
            this.logout();

            this.router.navigate(['/login']);
        });
    }

    private handleError (error: Response | any) {
        // In a real world app, you might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        return Observable.throw(errMsg);
    }
}