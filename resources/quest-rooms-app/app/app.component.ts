import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./../assets/sass/main.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent {}
