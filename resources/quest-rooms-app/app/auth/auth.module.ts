import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AuthRoutingModule } from './auth.routes';

import { LoginComponent } from './login/login.component';

@NgModule({
    declarations: [
        LoginComponent,
    ],
    imports: [
        AuthRoutingModule,
        FormsModule,
    ],
    providers: [],
})

export class AuthModule { }