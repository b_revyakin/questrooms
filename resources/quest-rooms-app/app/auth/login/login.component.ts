import { Component } from '@angular/core';
import {Router} from "@angular/router";
import { AuthService } from './../../services/auth.service';

@Component({
    templateUrl: './login.html',
    providers: [AuthService],
})

export class LoginComponent {
    form = {};

    constructor(private authService: AuthService, private router: Router) { }

    login() {
        this.authService.login(this.form).subscribe(() => {
            this.router.navigate(['/dashboard'])
        });
    }
}
