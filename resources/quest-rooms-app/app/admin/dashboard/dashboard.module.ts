import { NgModule } from '@angular/core';

import { DashboardRoutingModule } from './dashboard.routes';

import { DashboardComponent } from './dashboard.component';

@NgModule({
    declarations: [
        DashboardComponent,
    ],
    imports: [
        DashboardRoutingModule,
    ],
    providers: [],
})

export class DashboardModule { }