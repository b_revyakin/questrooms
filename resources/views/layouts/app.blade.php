<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>QuestRooms</title>
  <base href="/">

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
</head>
<body>

<app-root>Loading...</app-root>

<script type="text/javascript" src="/app/inline.bundle.js"></script>
<script type="text/javascript" src="/app/polyfills.bundle.js"></script>
<script type="text/javascript" src="/app/styles.bundle.js"></script>
<script type="text/javascript" src="/app/vendor.bundle.js"></script>
<script type="text/javascript" src="/app/main.bundle.js"></script>

</body>
</html>
